FROM node

WORKDIR /src

RUN npm install nodemon -g

EXPOSE 3000
CMD nodemon