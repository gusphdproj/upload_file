var express = require('express');
var fs = require('fs');
var path = require('path');
var os = require('os');
var config = require('./config');

var router = express.Router();

/* GET users listing. */
router.post('/file/', function(req, res) {
    var fstream;
    // var file_dir = tmp_dir;
    var form_parameters = {};
    var result = [];
    var number_of_files = 1;
    var counter = 0;
    // console.log(req.busboy)
    var metadata = {};

    req.pipe(req.busboy);

    req.busboy.on('field', function(fieldname, val) {
        console.log(fieldname);
    });

    req.busboy.on('file', function(fieldname, file, filename) {
        counter++;
        metadata[filename] = {
            local_file: Date.now(),
            filename: filename,

        };

        console.log("Uploading: " + metadata[filename].filename, metadata[filename].local_file);

        fstream = fs.createWriteStream(config.local_storage + metadata[filename].local_file);
        file.pipe(fstream);

        fstream.on('close', function() {
            counter--;
            if (counter == 0) {
                res.send(Object.values(metadata));
            };
        });
    });

    // req.busboy.on('finish', function() {

    //     res.send(result);
    //     console.log(result);
    //     // res.status(constants.HTTP_CODE_OK);
    //     // res.json({ "relativePath": relativePath });
    // });



});

module.exports = router;