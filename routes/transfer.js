var SSH = require('simple-ssh');
var fs = require('fs');
var exec = require('child_process').exec;
var config = require('./config');

var express = require('express');
var fs = require('fs');
var path = require('path');
var os = require('os');

var router = express.Router();

function transfer_data(local_file, remote_file) {
    // local_file = local_file.replace(" ", "_").replace("&", "_").replace("|", "_").replace(";", "_");

    var cmd = 'scp ' + local_file + " " + remote_file + ' && rm ' + local_file + ' &';
    exec(cmd, function(error, stdout, stderr) {
        console.log('Error:' + error);
        console.log('Stdout:' + stdout);
        console.log("Stderr:" + stderr);
    });
}

/* GET users listing. */
router.post('/file/', function(req, res) {

    local_file = req.body.local_file;
    if (!Number.isInteger(local_file)) {
        res.send({ status: "Error file is not allowed!" });
    }

    remote_file = config.remote_host + ":" + req.body.remote_dir + "/" + local_file;
    transfer_data(config.local_storage + local_file, remote_file);

    res.send({ remote_file: local_file });
});

router.get('/file/status/:remote_file', function(req, res) {

    remote_file = config.local_storage + req.params.remote_file;
    // console.log(remote_file);

    fs.exists(remote_file, function(exists) {
        if (exists) {
            res.send({ status: false });
        } else {
            res.send({ status: true });
        }
    });


});

module.exports = router;